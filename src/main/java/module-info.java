module io.gitlab.multicia.graph {
	requires transitive io.gitlab.multicia.jsonify;
	requires static transitive org.jetbrains.annotations;

	exports io.gitlab.multicia.graph;
}