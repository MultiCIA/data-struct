/*
 * Copyright (C) 2021-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.graph;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Map;
import java.util.function.Supplier;

/**
 * The {@link GraphHasher} calculate the hash value of an {@link Element} by calling the corresponding
 * {@link ElementHasher} and caching the results. The complexity of the infinite-depth version grows very quickly,
 * especially when the graph is dense. The upper bound complexity is O(n!). But since this hash value is only used for
 * quick comparison between multiple {@link Element}s (similar to what {@link Object#hashCode()} does), a depth limit is
 * implemented to make this calculation much faster.
 * <p>
 * Note: This interface should not be implemented by user.
 */
public sealed interface GraphHasher permits GraphDiffer.TypeDiffer {
	/**
	 * Get the {@link MatchingType} of this {@link GraphHasher}.
	 *
	 * @return The {@link MatchingType} of this {@link GraphHasher}.
	 */
	@NotNull MatchingType getMatchingType();

	/**
	 * Get the {@link GraphHasher} corresponding to this {@link MatchingType}.
	 *
	 * @param matchingType The {@link MatchingType}.
	 * @return The {@link GraphHasher} corresponding to this {@link MatchingType}.
	 */
	@NotNull GraphHasher getGraphHasher(@NotNull MatchingType matchingType);


	/**
	 * Wraps the given {@link Element} in an {@link ElementWrapper}, using this {@link MatchingType}. The
	 * {@link ElementWrapper}'s {@link Object#equals(Object)} is implemented via
	 * {@link GraphMatcher#match(Element, Element)}, and {@link Object#hashCode()} is implemented via
	 * {@link GraphHasher#hash(Element)}.
	 *
	 * @param element the {@link Element} to wrap.
	 * @return A {@link Supplier} providing the wrapper object.
	 */
	<E extends Element> @NotNull ElementWrapper<@Nullable E> wrap(@Nullable E element);

	/**
	 * Calculate the hash value of this {@link Element}.
	 *
	 * @param element Any {@link Element}.
	 * @param <E> Any {@link Element} type.
	 * @return The hash value of this {@link Element}.
	 */
	<E extends Element> int hash(@Nullable E element);

	/**
	 * Calculate the hash value of this {@link Collection}, assume that the ordering DOES matter.
	 *
	 * @param elements Any {@link Collection} of {@link Element}s.
	 * @param <E> Any {@link Element} type.
	 * @return The hash value of this {@link Collection}.
	 */
	<E extends Element> int hashOrdered(@Nullable Collection<@Nullable E> elements);

	/**
	 * Calculate the hash value of this {@link Collection}, assume that the ordering DOES NOT matter.
	 *
	 * @param elements Any {@link Collection} of {@link Element}s.
	 * @param <E> Any {@link Element} type.
	 * @return The hash value of this {@link Collection}.
	 */
	<E extends Element> int hashUnordered(@Nullable Collection<@Nullable E> elements);

	/**
	 * Calculate the hash value of this {@link Map}, assume that the ordering DOES matter.
	 *
	 * @param map Any {@link Map} of {@link Element}-to-{@link Element} entries.
	 * @param <K> Any {@link Element} type.
	 * @param <V> Any {@link Element} type.
	 * @return The hash value of this {@link Map}.
	 */
	<K extends Element, V extends Element> int hashOrdered(@Nullable Map<@Nullable K, @Nullable V> map);

	/**
	 * Calculate the hash value of this {@link Map}, assume that the ordering DOES NOT matter.
	 *
	 * @param map Any {@link Map} of {@link Element}-to-{@link Element} entries.
	 * @param <K> Any {@link Element} type.
	 * @param <V> Any {@link Element} type.
	 * @return The hash value of this {@link Map}.
	 */
	<K extends Element, V extends Element> int hashUnordered(@Nullable Map<@Nullable K, @Nullable V> map);
}
