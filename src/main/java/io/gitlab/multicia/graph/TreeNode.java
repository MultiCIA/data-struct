/*
 * Copyright (C) 2021-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.graph;

import io.gitlab.multicia.graph.ElementHasher.FieldHasher;
import io.gitlab.multicia.graph.ElementMatcher.FieldMatcher;
import io.gitlab.multicia.graph.GraphDeserializer.ArrayReader;
import io.gitlab.multicia.graph.GraphDeserializer.ObjectReader;
import io.gitlab.multicia.graph.GraphSerializer.ArrayWriter;
import io.gitlab.multicia.graph.GraphSerializer.ObjectWriter;
import org.jetbrains.annotations.MustBeInvokedByOverriders;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.UnmodifiableView;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

public class TreeNode extends Node {
	/**
	 * The signature of a {@link TreeNode}.
	 */
	public static final @NotNull Signature SIGNATURE = Node.SIGNATURE.suffix("TreeNode");

	/**
	 * The {@link FieldHasher} for the parent in the {@link TreeNode}.
	 */
	public static final @NotNull FieldHasher<@NotNull TreeNode> PARENT_HASHER
			= (hasher, node) -> hasher.hash(node.getParent());

	/**
	 * The {@link FieldMatcher} for the parent in the {@link TreeNode}.
	 */
	public static final @NotNull FieldMatcher<@NotNull TreeNode, @NotNull TreeNode> PARENT_MATCHER
			= (matcher, nodeA, nodeB) -> matcher.match(nodeA.getParent(), nodeB.getParent());

	/**
	 * The {@link FieldHasher} for the children (unordered) in the {@link TreeNode}.
	 */
	public static final @NotNull FieldHasher<@NotNull TreeNode> CHILDREN_UNORDERED_HASHER
			= (hasher, node) -> hasher.hashUnordered(node.getChildren());

	/**
	 * The {@link FieldMatcher} for the children (unordered) in the {@link TreeNode}.
	 */
	public static final @NotNull FieldMatcher<@NotNull TreeNode, @NotNull TreeNode> CHILDREN_UNORDERED_MATCHER
			= (matcher, nodeA, nodeB) -> matcher.matchUnordered(nodeA.getChildren(), nodeB.getChildren());

	/**
	 * The {@link FieldHasher} for the children (unordered) in the {@link TreeNode}.
	 */
	public static final @NotNull FieldHasher<@NotNull TreeNode> CHILDREN_ORDERED_HASHER
			= (hasher, node) -> hasher.hashOrdered(node.getChildren());

	/**
	 * The {@link FieldMatcher} for the children (unordered) in the {@link TreeNode}.
	 */
	public static final @NotNull FieldMatcher<@NotNull TreeNode, @NotNull TreeNode> CHILDREN_ORDERED_MATCHER
			= (matcher, nodeA, nodeB) -> matcher.matchOrdered(nodeA.getChildren(), nodeB.getChildren());


	/**
	 * The parent of this {@link TreeNode}.
	 */
	private @Nullable TreeNode parent = null;

	/**
	 * The children of this {@link TreeNode}.
	 */
	private final @NotNull Set<@NotNull TreeNode> children = new LinkedHashSet<>();


	/**
	 * Create a new {@link TreeNode}.
	 *
	 * @param signature The signature of the {@link TreeNode}.
	 */
	public TreeNode(@NotNull Signature signature) {
		super(signature);
		if (!signature.startsWith(SIGNATURE)) throw new GraphValueException("Invalid signature!");
	}

	/**
	 * Get the parent of this {@link TreeNode}.
	 *
	 * @return The parent {@link TreeNode}.
	 */
	public final @Nullable TreeNode getParent() {
		return parent;
	}

	/**
	 * Get the children of this {@link TreeNode}.
	 *
	 * @return The children {@link TreeNode}s.
	 */
	public final @NotNull @UnmodifiableView Set<@NotNull TreeNode> getChildren() {
		return Collections.unmodifiableSet(children);
	}

	/**
	 * Set the parent of this {@link TreeNode}.
	 *
	 * @param parent The parent {@link TreeNode}.
	 */
	public final void setParent(@Nullable TreeNode parent) {
		if (this.parent != null) {
			this.parent.children.remove(this);
		}
		this.parent = parent;
		if (parent != null) {
			parent.children.add(this);
		}
	}

	/**
	 * Add a child to this {@link TreeNode}.
	 *
	 * @param child The child {@link TreeNode}.
	 */
	public final void addChild(@NotNull TreeNode child) {
		child.setParent(this);
	}

	/**
	 * Remove a child from this {@link TreeNode}.
	 *
	 * @param child The child {@link TreeNode}.
	 */
	public final void removeChild(@NotNull TreeNode child) {
		if (child.getParent() == this) child.setParent(null);
	}


	@MustBeInvokedByOverriders
	@Override
	protected void writeToJson(@NotNull ObjectWriter writer) {
		super.writeToJson(writer);
		if (!children.isEmpty()) {
			final ArrayWriter childrenWriter = writer.putArray("children");
			for (final TreeNode node : children) {
				childrenWriter.addElement(node);
			}
		}
	}

	@MustBeInvokedByOverriders
	@Override
	protected void readFromJson(@NotNull ObjectReader reader) {
		super.readFromJson(reader);
		final ArrayReader childrenReader = reader.getAsArray("children");
		if (childrenReader != null) {
			for (int index = 0, length = childrenReader.size(); index < length; index++) {
				final TreeNode node = childrenReader.getAsElement(index, TreeNode.class);
				if (node == null) throw new GraphValueException("TreeNode contains null child!");
				node.setParent(this);
			}
		}
	}
}
