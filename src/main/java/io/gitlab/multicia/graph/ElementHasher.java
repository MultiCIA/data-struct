/*
 * Copyright (C) 2021-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.graph;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * {@link ElementHasher} calculates the hash value of an {@link Element}. This record should be created by the user to
 * implement the way the hash value is calculated for this type of {@link Element}. Implementer must make sure that any
 * matching {@link Element}s with the same {@link MatchingType} will have the same hash value. Any dependency to parent
 * type {@link ElementHasher} should be called directly. Any hash calculation for other {@link Element} or other type of
 * {@link MatchingType} should be called through {@link GraphHasher}.
 *
 * @param <E> Any {@link Element} type.
 */
public record ElementHasher<E extends Element>(
		@NotNull MatchingType matchingType,
		@NotNull Class<E> elementClass,
		@NotNull List<@NotNull FieldHasher<? super E>> fieldHashers
) {
	/**
	 * Calculate the hash of the specific {@link Element}. Any dependency to parent type {@link ElementHasher} should be
	 * called directly. Any hash calculation for other {@link Element} or other type of {@link MatchingType} should be
	 * called through {@link GraphHasher}.
	 *
	 * @param hasher The {@link GraphHasher}.
	 * @param element The specific {@link Element}.
	 * @return The hash value of the {@code element}.
	 */
	public int hash(@NotNull GraphHasher hasher, @NotNull E element) {
		int hash = 1;
		for (final FieldHasher<? super E> fieldHasher : fieldHashers) {
			hash = hash * 31 + fieldHasher.hash(hasher, element);
		}
		return hash;
	}


	@FunctionalInterface
	public interface FieldHasher<E extends Element> {
		/**
		 * Calculate the hash of some fields in a specific type of {@link Element}. Any hash calculation for other
		 * {@link Element} should be called through {@link GraphHasher}.
		 *
		 * @param hasher The {@link GraphHasher}.
		 * @param element The specific {@link Element}.
		 * @return The hash value of specified fields in the {@code element}.
		 */
		int hash(@NotNull GraphHasher hasher, @NotNull E element);
	}
}
