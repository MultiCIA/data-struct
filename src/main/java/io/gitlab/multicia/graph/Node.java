/*
 * Copyright (C) 2021-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.graph;

import io.gitlab.multicia.graph.ElementHasher.FieldHasher;
import io.gitlab.multicia.graph.ElementMatcher.FieldMatcher;
import io.gitlab.multicia.graph.GraphDeserializer.ArrayReader;
import io.gitlab.multicia.graph.GraphDeserializer.ObjectReader;
import io.gitlab.multicia.graph.GraphSerializer.ArrayWriter;
import io.gitlab.multicia.graph.GraphSerializer.ObjectWriter;
import org.jetbrains.annotations.MustBeInvokedByOverriders;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.UnmodifiableView;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public non-sealed class Node extends Element {
	/**
	 * The signature of a {@link Node}.
	 */
	public static final @NotNull Signature SIGNATURE = Signature.of("Node");

	/**
	 * The {@link FieldHasher} for the inbound connections in the {@link Node}.
	 */
	public static final @NotNull FieldHasher<@NotNull Node> INBOUND_CONNECTION_HASHER
			= (hasher, node) -> hasher.hashUnordered(node.getInboundConnections());

	/**
	 * The {@link FieldMatcher} for the inbound connections in the {@link Node}.
	 */
	public static final @NotNull FieldMatcher<@NotNull Node, @NotNull Node> INBOUND_CONNECTION_MATCHER
			= (matcher, nodeA, nodeB) -> matcher.matchUnordered(
			nodeA.getInboundConnections(), nodeB.getInboundConnections());

	/**
	 * The {@link FieldHasher} for the outbound connections in the {@link Node}.
	 */
	public static final @NotNull FieldHasher<@NotNull Node> OUTBOUND_CONNECTION_HASHER
			= (hasher, node) -> hasher.hashUnordered(node.getOutboundConnections());

	/**
	 * The {@link FieldMatcher} for the outbound connections in the {@link Node}.
	 */
	public static final @NotNull FieldMatcher<@NotNull Node, @NotNull Node> OUTBOUND_CONNECTION_MATCHER
			= (matcher, nodeA, nodeB) -> matcher.matchUnordered(
			nodeA.getInboundConnections(), nodeB.getOutboundConnections());


	/**
	 * The inbound connections. Map between source {@link Node} and their {@link Connections}.
	 */
	private final @NotNull Map<@NotNull Node, @NotNull Connections> inboundConnections = new LinkedHashMap<>();

	/**
	 * The inbound connections. Map between target {@link Node} and their {@link Connections}.
	 */
	private final @NotNull Map<@NotNull Node, @NotNull Connections> outboundConnections = new LinkedHashMap<>();


	/**
	 * Create a new {@link Node}.
	 *
	 * @param signature The signature of the {@link Node}.
	 */
	public Node(@NotNull Signature signature) {
		super(signature);
		if (!signature.startsWith(SIGNATURE)) throw new GraphValueException("Invalid signature!");
	}


	public final @UnmodifiableView @NotNull Map<@NotNull Node, @NotNull Connections> getInboundConnections() {
		return Collections.unmodifiableMap(inboundConnections);
	}

	public final @UnmodifiableView @NotNull Map<@NotNull Node, @NotNull Connections> getOutboundConnections() {
		return Collections.unmodifiableMap(outboundConnections);
	}


	public final @UnmodifiableView @NotNull Set<@NotNull Node> getSourceNodes() {
		return Collections.unmodifiableSet(inboundConnections.keySet());
	}

	public final @UnmodifiableView @NotNull Set<@NotNull Node> getTargetNodes() {
		return Collections.unmodifiableSet(outboundConnections.keySet());
	}


	public final @Nullable Connections getConnectionsFrom(@NotNull Node node) {
		return inboundConnections.get(node);
	}

	public final @Nullable Connections getConnectionsTo(@NotNull Node node) {
		return outboundConnections.get(node);
	}


	public final void addConnectionFrom(@NotNull Node from, @NotNull Connection type) {
		final Connections connections = inboundConnections.get(from);
		if (connections != null) {
			connections.addCount(type, 1);
		} else {
			final Connections newConnections = new Connections();
			newConnections.addCount(type, 1);
			inboundConnections.put(from, newConnections);
			from.outboundConnections.put(this, newConnections);
		}
	}

	public final void addConnectionTo(@NotNull Node to, @NotNull Connection type) {
		final Connections connections = outboundConnections.get(to);
		if (connections != null) {
			connections.addCount(type, 1);
		} else {
			final Connections newConnections = new Connections();
			newConnections.addCount(type, 1);
			outboundConnections.put(to, newConnections);
			to.inboundConnections.put(this, newConnections);
		}
	}


	public final boolean removeConnectionFrom(@NotNull Node from, @NotNull Connection type) {
		final Connections connections = inboundConnections.get(from);
		if (connections == null) return false;
		connections.subtractCount(type, 1);
		return true;
	}

	public final boolean removeConnectionTo(@NotNull Node to, @NotNull Connection type) {
		final Connections connections = outboundConnections.get(to);
		if (connections == null) return false;
		connections.subtractCount(type, 1);
		return true;
	}


	public final void addConnectionsFrom(@NotNull Node from, @NotNull Connections connections) {
		final Connections currentConnections = inboundConnections.get(from);
		if (from.outboundConnections.get(this) != currentConnections) throw new AssertionError();
		if (currentConnections != null) {
			currentConnections.addAllCounts(connections);
		} else {
			final Connections newConnections = new Connections(connections);
			inboundConnections.put(from, newConnections);
			from.outboundConnections.put(this, newConnections);
		}
	}

	public final void addConnectionsTo(@NotNull Node to, @NotNull Connections connections) {
		final Connections currentConnections = outboundConnections.get(to);
		if (to.inboundConnections.get(this) != currentConnections) throw new AssertionError();
		if (currentConnections != null) {
			currentConnections.addAllCounts(connections);
		} else {
			final Connections newConnections = new Connections(connections);
			outboundConnections.put(to, newConnections);
			to.inboundConnections.put(this, newConnections);
		}
	}


	public final void setConnectionsFrom(@NotNull Node from, @NotNull Connections connections) {
		final Connections newConnections = new Connections(connections);
		inboundConnections.put(from, newConnections);
		from.outboundConnections.put(this, newConnections);
	}

	public final void setConnectionsTo(@NotNull Node to, @NotNull Connections connections) {
		final Connections newConnections = new Connections(connections);
		outboundConnections.put(to, newConnections);
		to.inboundConnections.put(this, newConnections);
	}


	public final @Nullable Connections removeConnectionsFrom(@NotNull Node from) {
		final Connections connections = inboundConnections.remove(from);
		if (from.outboundConnections.remove(this) != connections) throw new AssertionError();
		return connections;
	}

	public final @Nullable Connections removeConnectionsTo(@NotNull Node to) {
		final Connections connections = outboundConnections.remove(to);
		if (to.inboundConnections.remove(this) != connections) throw new AssertionError();
		return connections;
	}


	@MustBeInvokedByOverriders
	@Override
	protected void writeToJson(@NotNull ObjectWriter writer) {
		super.writeToJson(writer);
		if (!outboundConnections.isEmpty()) {
			final ArrayWriter outboundConnectionsWriter = writer.putArray("outboundConnections");
			for (final Map.Entry<Node, Connections> entry : outboundConnections.entrySet()) {
				final Node node = entry.getKey();
				outboundConnectionsWriter.addElement(node);

				final Connections connections = entry.getValue();
				outboundConnectionsWriter.addElement(connections);
			}
		}
	}

	@MustBeInvokedByOverriders
	@Override
	protected void readFromJson(@NotNull ObjectReader reader) {
		super.readFromJson(reader);
		final ArrayReader outboundConnectionsReader = reader.getAsArray("outboundConnections");
		if (outboundConnectionsReader != null) {
			final int size = outboundConnectionsReader.size();
			if ((size & 1) != 0) throw new GraphValueException("Invalid outbound connections array size!");

			for (int index = 0; index < size; index += 2) {
				final Node node = outboundConnectionsReader.getAsElement(index, Node.class);
				if (node == null) throw new GraphValueException("Unexpected null value!");

				final Connections connections = outboundConnectionsReader.getAsElement(index + 1, Connections.class);
				if (connections == null) throw new GraphValueException("Unexpected null value!");

				outboundConnections.put(node, connections);
				node.inboundConnections.put(this, connections);
			}
		}
	}


	/**
	 * The only {@link Node} that equal to this {@link Node} is this {@link Node} itself. This makes all collections of
	 * {@link Node}s effectively become identity collections.
	 *
	 * @param object object
	 * @return {@code this == object}
	 */
	@Override
	public final boolean equals(@Nullable Object object) {
		return this == object;
	}

	/**
	 * Return the object's default hashcode.
	 *
	 * @return {@code super.hashcode()}
	 */
	@Override
	public final int hashCode() {
		return super.hashCode();
	}
}
