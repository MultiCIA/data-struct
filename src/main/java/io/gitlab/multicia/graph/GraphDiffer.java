/*
 * Copyright (C) 2021-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.graph;

import org.jetbrains.annotations.ApiStatus;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Unmodifiable;

import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

/**
 * The Graph comparison toolset. Currently, the API set is in experimental state.
 */
@ApiStatus.Experimental
public final class GraphDiffer {
	private final @NotNull Map<@NotNull MatchingType, @NotNull TypeDiffer> typeDiffers = new HashMap<>();

	public GraphDiffer(@NotNull Set<@NotNull ElementHasher<?>> hashers,
			@NotNull Set<@NotNull ElementMatcher<?, ?>> matchers) {
		for (final ElementHasher<?> hasher : hashers) {
			typeDiffers.computeIfAbsent(hasher.matchingType(), TypeDiffer::new).addElementHasher(hasher);
		}
		for (final ElementMatcher<?, ?> matcher : matchers) {
			typeDiffers.computeIfAbsent(matcher.matchingType(), TypeDiffer::new).addElementMatcher(matcher);
		}
	}

	public @Unmodifiable @NotNull Collection<@NotNull Set<@NotNull Node>> compareNodes(
			@NotNull MatchingType matchingType, @NotNull Graph graphA, @NotNull Graph graphB) {
		final TypeDiffer typeDiffer = typeDiffers.get(matchingType);
		if (typeDiffer == null) {
			throw new GraphDifferException("Matching type %s is not registered in this context."
					.formatted(matchingType.getName()));
		}
		final Map<Object, Set<Node>> map = new HashMap<>();
		for (final Group<?> group : graphA) {
			for (final Node node : group) {
				map.computeIfAbsent(typeDiffer.wrap(node), GraphDiffer::createSet).add(node);
			}
		}
		for (final Group<?> group : graphB) {
			for (final Node node : group) {
				map.computeIfAbsent(typeDiffer.wrap(node), GraphDiffer::createSet).add(node);
			}
		}
		return Set.copyOf(map.values());
	}

	private static <A, B> @NotNull Set<@NotNull B> createSet(@Nullable A any) {
		return new HashSet<>();
	}


	final class TypeDiffer implements GraphHasher, GraphMatcher {
		private final @NotNull MatchingType matchingType;

		private TypeDiffer(@NotNull MatchingType matchingType) {
			this.matchingType = matchingType;
		}

		private static <E> @NotNull Twin<@NotNull E> unorderedKey(@NotNull E a, @NotNull E b) {
			return a.hashCode() <= b.hashCode() ? new Twin<>(a, b) : new Twin<>(b, a);
		}

		@Override
		public @NotNull MatchingType getMatchingType() {
			return matchingType;
		}

		private @NotNull TypeDiffer getTypeDiffer(@NotNull MatchingType matchingType) {
			if (this.matchingType.equals(matchingType)) return this;
			final TypeDiffer typeDiffer = typeDiffers.get(matchingType);
			if (typeDiffer == null) {
				throw new GraphDifferException("Matching type %s is not registered in this context."
						.formatted(matchingType.getName()));
			}
			return typeDiffer;
		}

		@Override
		public <E extends Element> @NotNull ElementWrapper<@Nullable E> wrap(@Nullable E element) {
			return new ElementWrapper<>(this, element);
		}

		//region Hasher

		private final @NotNull Map<@NotNull Class<?>, @NotNull ElementHasher<?>> elementHashers = new HashMap<>();

		/**
		 * The depth limit value for the hash calculation. The minimum value allowed is 1. Increasing the depth limit
		 * will improve the accuracy of the hash, but will also increase the computational expense.
		 */
		private static final int DEPTH_LIMIT = 5;

		/**
		 * The stack of multiple layers of independent cache of hashing value, where each layer's hashing result is not
		 * shared with the previous layer. This is necessary because the hash value of an {@link Element} is dependent
		 * on the previous hashing {@link Element}s.
		 */
		private final @NotNull Deque<@NotNull Map<@NotNull Element, @NotNull Integer>> hashCacheStack
				= new LinkedList<>();

		/**
		 * The hashing {@link Element}s.
		 */
		private final @NotNull Set<@NotNull Element> hashingElements = new HashSet<>();

		// Initialize hash cache stack
		{
			hashCacheStack.push(new HashMap<>());
		}

		private void addElementHasher(@NotNull ElementHasher<?> elementHasher) {
			assert elementHasher.matchingType() == matchingType;
			final Class<?> nodeClass = elementHasher.elementClass();
			final ElementHasher<?> previousHasher = elementHashers.put(nodeClass, elementHasher);
			if (previousHasher != null) {
				throw new IllegalArgumentException("Duplicate NodeHasher for matching type %s with node class %s!"
						.formatted(matchingType, nodeClass.getName()));
			}
		}

		@Override
		public @NotNull GraphHasher getGraphHasher(@NotNull MatchingType matchingType) {
			return getTypeDiffer(matchingType);
		}

		@Override
		public <E extends Element> int hash(@Nullable E element) {
			// Null check
			if (element == null) return 0;

			// Loop breaker
			if (hashingElements.contains(element)) return -1;

			// get current cache layer, check if already calculated
			final Map<Element, Integer> hashCache = hashCacheStack.getLast();
			final Integer cached = hashCache.get(element);
			if (cached != null) return cached;

			// check the depth limit
			if (hashingElements.size() >= DEPTH_LIMIT) return 1;

			// Create new cache layer
			hashCacheStack.push(new HashMap<>());
			hashingElements.add(element);

			// find element hasher
			final Class<?> elementClass = element.getClass();
			@SuppressWarnings("unchecked") final ElementHasher<E> elementHasher
					= (ElementHasher<E>) elementHashers.get(elementClass);
			if (elementHasher == null) {
				throw new GraphDifferException("Cannot find ElementHasher for matching type %s with element class %s!"
						.formatted(matchingType, elementClass.getName()));
			}

			// calculate the hash value of the element at this layer
			final int hash = elementHasher.hash(this, element);

			// remove new cache layer
			hashingElements.remove(element);
			hashCacheStack.pop();

			// add result to current level cache
			hashCache.put(element, hash);
			return hash;
		}

		@Override
		public <E extends Element> int hashOrdered(@Nullable Collection<@Nullable E> elements) {
			if (elements == null) return 0;

			int hash = 1;
			for (final E element : elements) {
				hash = hash * 31 + hash(element);
			}
			return hash * 31 + elements.size();
		}

		@Override
		public <E extends Element> int hashUnordered(@Nullable Collection<@Nullable E> elements) {
			if (elements == null) return 0;

			int hash = 1;
			for (final E element : elements) {
				hash += hash(element);
			}
			return hash * 31 + elements.size();
		}

		@Override
		public <K extends Element, V extends Element> int hashOrdered(@Nullable Map<@Nullable K, @Nullable V> map) {
			if (map == null) return 0;

			int hash = 1;
			for (final Map.Entry<K, V> entry : map.entrySet()) {
				hash = hash * 31 + hash(entry.getKey());
				hash = hash * 31 + hash(entry.getValue());
			}
			return hash * 31 + map.size();
		}

		@Override
		public <K extends Element, V extends Element> int hashUnordered(@Nullable Map<@Nullable K, @Nullable V> map) {
			if (map == null) return 0;

			int hash = 1;
			for (final Map.Entry<K, V> entry : map.entrySet()) {
				hash += hash(entry.getKey()) * 31 + hash(entry.getValue());
			}
			return hash * 31 + map.size();
		}

		//endregion Hasher

		//region Matcher

		private final @NotNull Map<@NotNull Twin<@NotNull Class<?>>, @NotNull ElementMatcher<?, ?>> nodeMatchers
				= new HashMap<>();

		private final @NotNull Deque<@NotNull Set<@NotNull Twin<@NotNull Element>>> positiveCacheStack
				= new LinkedList<>();
		private final @NotNull Set<@NotNull Twin<@NotNull Element>> negativeCache = new HashSet<>();

		// Initialize match positive cache stack
		{
			positiveCacheStack.push(new HashSet<>());
		}

		private void addElementMatcher(@NotNull ElementMatcher<?, ?> elementMatcher) {
			assert elementMatcher.matchingType() == matchingType;
			final Class<?> nodeClassA = elementMatcher.elementClassA();
			final Class<?> nodeClassB = elementMatcher.elementClassB();
			final Twin<Class<?>> twin = unorderedKey(nodeClassA, nodeClassB);
			final ElementMatcher<?, ?> previousMatcher = nodeMatchers.put(twin, elementMatcher);
			if (previousMatcher != null) {
				throw new IllegalArgumentException("Duplicate NodeMatcher for matching type %s with node classes %s and %s!"
						.formatted(matchingType, nodeClassA.getName(), nodeClassB.getName()));
			}
		}

		@Override
		public @NotNull GraphMatcher getGraphMatcher(@NotNull MatchingType matchingType) {
			return getTypeDiffer(matchingType);
		}

		@Override
		public <A extends Element, B extends Element> boolean match(@Nullable A elementA, @Nullable B elementB) {
			if (elementA == elementB) return true;
			if (elementA == null || elementB == null) return false;
			if (hash(elementA) != hash(elementB)) return false;

			// This key will guarantee to work even if graphA == graphB
			final Twin<Element> key = unorderedKey(elementA, elementB);

			// Check negative cache
			if (negativeCache.contains(key)) return false;

			// Check multi-layered positive cache
			for (final Set<Twin<Element>> positiveCache : positiveCacheStack) {
				if (positiveCache.contains(key)) return true;
			}

			// create new layer
			final Set<Twin<Element>> newPositiveCache = new HashSet<>();
			positiveCacheStack.addLast(newPositiveCache);

			// theory: current comparison is true
			newPositiveCache.add(key);

			// find element matcher
			final Class<?> elementClassA = elementA.getClass();
			final Class<?> elementClassB = elementB.getClass();
			final Twin<Class<?>> matchersKey = unorderedKey(elementClassA, elementClassB);
			final ElementMatcher<?, ?> elementMatcher = nodeMatchers.get(matchersKey);
			if (elementMatcher == null) {
				throw new GraphDifferException(
						"Cannot find NodeMatcher for matching type %s with node classes %s and %s!"
								.formatted(matchingType, elementClassA.getName(), elementClassB.getName()));
			}

			// calculate and check the theory
			@SuppressWarnings("unchecked") final boolean result = elementMatcher.elementClassA() == elementClassA
					? ((ElementMatcher<A, B>) elementMatcher).match(this, elementA, elementB)
					: ((ElementMatcher<B, A>) elementMatcher).match(this, elementB, elementA);

			// remove new layer
			positiveCacheStack.removeLast();

			if (result) {
				// theory is correct, combine result with previous level
				positiveCacheStack.getLast().addAll(newPositiveCache);
				return true;
			} else {
				// theory is incorrect, add to negative cache
				negativeCache.add(key);
				return false;
			}
		}

		@Override
		public <A extends Element, B extends Element> boolean matchOrdered(
				@Nullable Collection<@Nullable A> elementsA,
				@Nullable Collection<@Nullable B> elementsB
		) {
			if (elementsA == elementsB) return true;
			if (elementsA == null || elementsB == null) return false;
			if (elementsA.size() != elementsB.size()) return false;
			final Iterator<A> iteratorA = elementsA.iterator();
			final Iterator<B> iteratorB = elementsB.iterator();
			while (iteratorA.hasNext()/* && iteratorB.hasNext()*/) {
				if (!match(iteratorA.next(), iteratorB.next())) return false;
			}
			return true;
		}

		@Override
		public <A extends Element, B extends Element> boolean matchUnordered(
				@Nullable Collection<@Nullable A> elementsA,
				@Nullable Collection<@Nullable B> elementsB
		) {
			if (elementsA == elementsB) return true;
			if (elementsA == null || elementsB == null) return false;
			if (elementsA.size() != elementsB.size()) return false;
			final Map<ElementWrapper<?>, Integer> map = new HashMap<>();
			for (final Element node : elementsA) {
				final ElementWrapper<?> wrapper = wrap(node);
				map.merge(wrapper, 1, Integer::sum);
			}
			for (final Element node : elementsB) {
				final ElementWrapper<?> wrapper = wrap(node);
				if (map.merge(wrapper, -1, Integer::sum) < 0) return false;
			}
			return true;
		}

		@Override
		public <KA extends Element, VA extends Element, KB extends Element, VB extends Element> boolean matchOrdered(
				@Nullable Map<@Nullable KA, @Nullable VA> mapA,
				@Nullable Map<@Nullable KB, @Nullable VB> mapB
		) {
			if (mapA == mapB) return true;
			if (mapA == null || mapB == null) return false;
			if (mapA.size() != mapB.size()) return false;
			final Iterator<Map.Entry<KA, VA>> iteratorA = mapA.entrySet().iterator();
			final Iterator<Map.Entry<KB, VB>> iteratorB = mapB.entrySet().iterator();
			while (iteratorA.hasNext()/* && iteratorB.hasNext()*/) {
				final Map.Entry<KA, VA> entryA = iteratorA.next();
				final Map.Entry<KB, VB> entryB = iteratorB.next();
				if (!match(entryA.getKey(), entryB.getKey())) return false;
				if (!match(entryA.getValue(), entryB.getValue())) return false;
			}
			return true;
		}

		@Override
		public <KA extends Element, VA extends Element, KB extends Element, VB extends Element> boolean matchUnordered(
				@Nullable Map<@Nullable KA, @Nullable VA> mapA,
				@Nullable Map<@Nullable KB, @Nullable VB> mapB
		) {
			if (mapA == mapB) return true;
			if (mapA == null || mapB == null) return false;
			if (mapA.size() != mapB.size()) return false;
			final Map<Twin<ElementWrapper<?>>, Integer> map = new HashMap<>();
			for (final Map.Entry<KA, VA> entry : mapA.entrySet()) {
				final ElementWrapper<?> keyWrapper = wrap(entry.getKey());
				final ElementWrapper<?> valueWrapper = wrap(entry.getValue());
				final Twin<ElementWrapper<?>> twin = new Twin<>(keyWrapper, valueWrapper);
				map.merge(twin, 1, Integer::sum);
			}
			for (final Map.Entry<KB, VB> entry : mapB.entrySet()) {
				final ElementWrapper<?> keyWrapper = wrap(entry.getKey());
				final ElementWrapper<?> valueWrapper = wrap(entry.getValue());
				final Twin<ElementWrapper<?>> twin = new Twin<>(keyWrapper, valueWrapper);
				if (map.merge(twin, -1, Integer::sum) < 0) return false;
			}
			return true;
		}

		//endregion Matcher
	}

	private record Twin<E>(@NotNull E one, @NotNull E two) {
	}
}
