/*
 * Copyright (C) 2021-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.graph;

import io.gitlab.multicia.graph.ElementHasher.FieldHasher;
import io.gitlab.multicia.graph.ElementMatcher.FieldMatcher;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.SequencedSet;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * This is the base class for an {@link Element} that contains other {@link Element}s.
 *
 * @param <E> The type of Element that this class can contain.
 */
public sealed abstract class ElementContainer<E extends Element> extends Element implements SequencedSet<E>
		permits Graph, Group {
	/**
	 * The {@link FieldHasher} for the elements (unordered) in the {@link ElementContainer}.
	 */
	public static final @NotNull FieldHasher<@NotNull ElementContainer<?>> ELEMENTS_UNORDERED_HASHER
			= GraphHasher::hashUnordered;

	/**
	 * The {@link FieldMatcher} for the elements (unordered) in the {@link ElementContainer}.
	 */
	public static final @NotNull FieldMatcher<@NotNull ElementContainer<?>, @NotNull ElementContainer<?>>
			ELEMENTS_UNORDERED_MATCHER = GraphMatcher::matchUnordered;

	/**
	 * The {@link FieldHasher} for the elements (ordered) in the {@link ElementContainer}.
	 */
	public static final @NotNull FieldHasher<@NotNull ElementContainer<?>> ELEMENTS_ORDERED_HASHER
			= GraphHasher::hashOrdered;

	/**
	 * The {@link FieldMatcher} for the elements (ordered) in the {@link ElementContainer}.
	 */
	public static final @NotNull FieldMatcher<@NotNull ElementContainer<?>, @NotNull ElementContainer<?>>
			ELEMENTS_ORDERED_MATCHER = GraphMatcher::matchOrdered;


	/**
	 * A {@link SequencedSet} of {@link Element}s that this {@link ElementContainer} contains.
	 * <p>
	 * Note: This field is handled specially by the JSON  serializer/deserializer.
	 */
	private final @NotNull SequencedSet<@NotNull E> elements = new LinkedHashSet<>();

	/**
	 * The type of {@link Element} that this class can contain. This field is not serialized to JSON.
	 */
	private final @NotNull Class<@NotNull E> elementClass;


	/**
	 * Create a new {@link ElementContainer}.
	 *
	 * @param signature The signature of the {@link ElementContainer}.
	 * @param elementClass The type of {@link Element} that this class can contain.
	 */
	@SuppressWarnings("unchecked")
	ElementContainer(@NotNull Signature signature, @NotNull Class<?> elementClass) {
		super(signature);
		this.elementClass = (Class<E>) elementClass;
	}

	@NotNull Class<@NotNull E> getElementClass() {
		return elementClass;
	}


	//region SequencedSet

	@Override
	public final @NotNull Spliterator<@NotNull E> spliterator() {
		return elements.spliterator();
	}

	@Override
	public final @NotNull Iterator<@NotNull E> iterator() {
		return elements.iterator();
	}

	@Override
	public final void addFirst(@NotNull E element) {
		elements.addFirst(element);
	}

	@Override
	public final void addLast(@NotNull E element) {
		elements.addLast(element);
	}

	@Override
	public final @NotNull E getFirst() {
		return elements.getFirst();
	}

	@Override
	public final @NotNull E getLast() {
		return elements.getLast();
	}

	@Override
	public final @NotNull E removeFirst() {
		return elements.removeFirst();
	}

	@Override
	public final @NotNull E removeLast() {
		return elements.removeLast();
	}

	@Override
	public final boolean add(@NotNull E element) {
		return elements.add(element);
	}

	@Override
	public final boolean remove(@NotNull Object object) {
		return elements.remove(object);
	}

	@Override
	public final boolean addAll(@NotNull Collection<? extends E> collection) {
		return elements.addAll(collection);
	}

	@Override
	public final boolean retainAll(@NotNull Collection<?> collection) {
		return elements.retainAll(collection);
	}

	@Override
	public final boolean removeAll(@NotNull Collection<?> collection) {
		return elements.removeAll(collection);
	}

	@Override
	public final boolean removeIf(@NotNull Predicate<? super E> filter) {
		return elements.removeIf(filter);
	}

	@Override
	public final void clear() {
		elements.clear();
	}

	@Override
	public final @NotNull Object @NotNull [] toArray() {
		return elements.toArray();
	}

	@Override
	public final <T> @NotNull T @NotNull [] toArray(@NotNull T @NotNull [] array) {
		return elements.toArray(array);
	}

	@Override
	public final <T> @NotNull T @NotNull [] toArray(@NotNull IntFunction<T[]> generator) {
		return elements.toArray(generator);
	}

	@Override
	public final @NotNull Stream<@NotNull E> stream() {
		return elements.stream();
	}

	@Override
	public final @NotNull Stream<@NotNull E> parallelStream() {
		return elements.parallelStream();
	}

	@Override
	public final int size() {
		return elements.size();
	}

	@Override
	public final boolean isEmpty() {
		return elements.isEmpty();
	}

	@Override
	public final boolean contains(@Nullable Object object) {
		return elements.contains(object);
	}

	@Override
	public final boolean containsAll(@NotNull Collection<?> collection) {
		return elements.containsAll(collection);
	}

	@Override
	public final void forEach(@NotNull Consumer<? super E> action) {
		elements.forEach(action);
	}

	@Override
	public final @NotNull SequencedSet<@NotNull E> reversed() {
		return elements.reversed();
	}

	//endregion SequencedSet
}
