/*
 * Copyright (C) 2021-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.graph;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Map;
import java.util.function.Supplier;

/**
 * The {@link GraphMatcher} compare {@link Element}s  by calling the corresponding {@link ElementMatcher} and caching
 * the results. The complexity of this comparison grows very quickly, especially when the graph is dense. The upper
 * bound complexity is O(n!).
 * <p>
 * Note: This interface should not be implemented by user.
 */
public sealed interface GraphMatcher permits GraphDiffer.TypeDiffer {
	/**
	 * Get the {@link MatchingType} of this {@link GraphMatcher}.
	 *
	 * @return The {@link MatchingType} of this {@link GraphMatcher}.
	 */
	@NotNull MatchingType getMatchingType();

	/**
	 * Get the {@link GraphMatcher} corresponding to this {@link MatchingType}.
	 *
	 * @param matchingType The {@link MatchingType}.
	 * @return The {@link GraphMatcher} corresponding to this {@link MatchingType}.
	 */
	@NotNull GraphMatcher getGraphMatcher(@NotNull MatchingType matchingType);

	/**
	 * Wraps the given {@link Element} in an {@link ElementWrapper}, using this {@link MatchingType}. The
	 * {@link ElementWrapper}'s {@link Object#equals(Object)} is implemented via
	 * {@link GraphMatcher#match(Element, Element)}, and {@link Object#hashCode()} is implemented via
	 * {@link GraphHasher#hash(Element)}.
	 *
	 * @param element the {@link Element} to wrap.
	 * @return A {@link Supplier} providing the wrapper object.
	 */
	<E extends Element> @NotNull ElementWrapper<@Nullable E> wrap(@Nullable E element);

	/**
	 * Compare a pair of {@link Element}s.
	 *
	 * @param elementA The first {@link Element}.
	 * @param elementB The second {@link Element}.
	 * @param <A> Any {@link Element} type.
	 * @param <B> Any {@link Element} type.
	 * @return {@code true} if {@code elementA} matches {@code elementB}.
	 */
	<A extends Element, B extends Element> boolean match(@Nullable A elementA, @Nullable B elementB);

	/**
	 * Compare a pair of {@link Collection}s, assume that the ordering DOES matter.
	 *
	 * @param elementsA The first {@link Collection}.
	 * @param elementsB The second {@link Collection}.
	 * @param <A> Any {@link Element} type.
	 * @param <B> Any {@link Element} type.
	 * @return {@code true} if {@code elementsA} matches {@code elementsB}.
	 */
	<A extends Element, B extends Element> boolean matchOrdered(@Nullable Collection<@Nullable A> elementsA,
			@NotNull Collection<@Nullable B> elementsB);

	/**
	 * Compare a pair of {@link Collection}s, assume that the ordering DOES NOT matter.
	 *
	 * @param elementsA The first {@link Collection}.
	 * @param elementsB The second {@link Collection}.
	 * @param <A> Any {@link Element} type.
	 * @param <B> Any {@link Element} type.
	 * @return {@code true} if {@code elementsA} matches {@code elementsB}.
	 */
	<A extends Element, B extends Element> boolean matchUnordered(@Nullable Collection<@Nullable A> elementsA,
			@NotNull Collection<@Nullable B> elementsB);

	/**
	 * Compare a pair of {@link Map}s, assume that the ordering DOES matter.
	 *
	 * @param mapA The first {@link Map}.
	 * @param mapB The second {@link Map}.
	 * @param <KA> Any {@link Element} type.
	 * @param <VA> Any {@link Element} type.
	 * @param <KB> Any {@link Element} type.
	 * @param <VB> Any {@link Element} type.
	 * @return {@code true} if {@code mapA} matches {@code mapB}.
	 */
	<KA extends Element, VA extends Element, KB extends Element, VB extends Element> boolean matchOrdered(
			@Nullable Map<@Nullable KA, @Nullable VA> mapA, @Nullable Map<@Nullable KB, @Nullable VB> mapB);

	/**
	 * Compare a pair of {@link Map}s, assume that the ordering DOES NOT matter.
	 *
	 * @param mapA The first {@link Map}.
	 * @param mapB The second {@link Map}.
	 * @param <KA> Any {@link Element} type.
	 * @param <VA> Any {@link Element} type.
	 * @param <KB> Any {@link Element} type.
	 * @param <VB> Any {@link Element} type.
	 * @return {@code true} if {@code mapA} matches {@code mapB}.
	 */
	<KA extends Element, VA extends Element, KB extends Element, VB extends Element> boolean matchUnordered(
			@Nullable Map<@Nullable KA, @Nullable VA> mapA, @Nullable Map<@Nullable KB, @Nullable VB> mapB);
}
