/*
 * Copyright (C) 2021-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.graph;

import io.gitlab.multicia.jsonify.JsonArray;
import io.gitlab.multicia.jsonify.JsonElement;
import io.gitlab.multicia.jsonify.JsonKeyword;
import io.gitlab.multicia.jsonify.JsonNumber;
import io.gitlab.multicia.jsonify.JsonObject;
import io.gitlab.multicia.jsonify.JsonRuntimeException;
import io.gitlab.multicia.jsonify.JsonString;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;

public final class GraphSerializer {
	private final @NotNull Map<@NotNull Element, @NotNull Entry<@NotNull JsonObject, @Nullable JsonObject>> elementMap
			= new HashMap<>();
	private final @NotNull Map<@NotNull Object, @NotNull JsonElement> literalValueCache = new HashMap<>();


	private GraphSerializer() {
	}


	public static @NotNull JsonElement serialize(@NotNull Graph graph) throws GraphException {
		// split into 2 pass
		// 1. Create all JsonObject and their reference JsonArray from Element and Elements
		// 2. Serialize all Element to corresponding JsonObject
		try {
			final GraphSerializer serializer = new GraphSerializer();
			serializer.createAllReferencable(graph, new ArrayList<>());
			final JsonObject graphObject = new JsonObject();
			serializer.writeElementToJsonObject(graph, graphObject);
			return graphObject;
		} catch (final JsonRuntimeException | GraphValueException exception) {
			throw new GraphException("Cannot serialize this Graph to JSON!", exception);
		}
	}

	/**
	 * Create and cache json literal using a little hack ;)
	 */
	private <E> @NotNull JsonElement createCachedLiteral(@NotNull E value,
			@NotNull Function<@NotNull E, @NotNull JsonElement> function) {
		@SuppressWarnings("unchecked") final Map<E, JsonElement> cache = (Map<E, JsonElement>) literalValueCache;
		return cache.computeIfAbsent(value, function);
	}

	/**
	 * Create all referencable JsonObject for element.
	 */
	private <E extends Element> void createAllReferencable(@NotNull ElementContainer<@NotNull E> container,
			@NotNull List<@NotNull Long> referenceIndexes) {
		final int position = referenceIndexes.size();
		long index = 0;
		referenceIndexes.add(0L);
		for (final E element : container) {
			// set index value & increase index
			referenceIndexes.set(position, index++);
			// create reference
			final JsonObject reference = new JsonObject();
			final JsonArray referenceArray = new JsonArray();
			reference.put("", referenceArray);
			for (final Long referenceIndex : referenceIndexes) {
				referenceArray.add(createCachedLiteral(referenceIndex, JsonNumber::new));
			}
			// create element map entry
			elementMap.put(element, new SimpleImmutableEntry<>(new JsonObject(), reference));
			// recursive create
			if (element instanceof ElementContainer<?> innerContainer) {
				createAllReferencable(innerContainer, referenceIndexes);
			}
		}
		referenceIndexes.remove(position);
	}


	/**
	 * If input element can be reference, return a reference. If not, serialize it and return an object.
	 */
	private @NotNull JsonElement createFromElement(@NotNull Element element) {
		final Entry<JsonObject, JsonObject> entry = elementMap.get(element);
		if (entry != null) {
			// already serialized
			final JsonObject reference = entry.getValue();
			return reference != null
					? reference // element can be referenced
					: entry.getKey(); // element cannot be referenced
		} else if (element instanceof Entity) {
			// not serialized yet, also cannot be referenced
			final JsonObject object = new JsonObject();
			elementMap.put(element, new SimpleImmutableEntry<>(object, null));
			writeElementToJsonObject(element, object);
			return object;
		} else {
			throw new GraphValueException("Unknown element!");
		}
	}

	/**
	 * Write an {@link Element} to {@link JsonObject}. Handle {@link Element#getSignature()},
	 * {@link Element#unknownFields} and {@link ElementContainer#iterator()} here.
	 */
	private void writeElementToJsonObject(@NotNull Element element, @NotNull JsonObject object) {
		// write signature
		object.put("", createCachedLiteral(element.getSignature().toString(), JsonString::new));
		// write object using writeToJson method
		final ObjectWriter writer = new ObjectWriter(object);
		element.writeToJson(writer);
		// write inner elements
		if (element instanceof ElementContainer<?> container) {
			final ArrayWriter arrayWriter = writer.putArray("elements");
			for (final Element innerElement : container) {
				final Entry<JsonObject, JsonObject> entry = elementMap.get(innerElement);
				if (entry == null) throw new GraphValueException("Unknown element!");
				final JsonObject innerObject = entry.getKey();
				arrayWriter.addJson(innerObject);
				writeElementToJsonObject(innerElement, innerObject);
			}
		}
		// write unknown fields
		final Map<String, Object> unknownFields = element.getUnknownFields();
		for (final Entry<String, Object> entry : unknownFields.entrySet()) {
			writer.putRaw(entry.getKey(), entry.getValue());
		}
	}

	/**
	 * Create JsonElement from generic Object.
	 */
	private @NotNull JsonElement createFromObject(@Nullable Object object,
			@Nullable Map<@NotNull Object, @NotNull Boolean> identitySet) {
		return switch (object) {
			case null -> JsonKeyword.NULL;
			case JsonElement element -> {
				checkElement(element, identitySet);
				yield element;
			}
			case Boolean value -> createCachedLiteral(value, JsonElement::of);
			case String value -> createCachedLiteral(value, JsonString::new);
			case Character value -> createCachedLiteral(value.toString(), JsonString::new);
			case Byte value -> createCachedLiteral(value.longValue(), JsonNumber::new);
			case Short value -> createCachedLiteral(value.longValue(), JsonNumber::new);
			case Integer value -> createCachedLiteral(value.longValue(), JsonNumber::new);
			case Long value -> createCachedLiteral(value, JsonNumber::new);
			case Float value -> createCachedLiteral(value.doubleValue(), JsonNumber::new);
			case Double value -> createCachedLiteral(value, JsonNumber::new);
			case BigInteger value -> createCachedLiteral(value, JsonNumber::new);
			case BigDecimal value -> createCachedLiteral(value, JsonNumber::new);
			case Element element -> createFromElement(element);
			case Collection<?> collection -> {
				final Map<Object, Boolean> set = identitySet != null ? identitySet : new IdentityHashMap<>();
				if (set.put(collection, Boolean.TRUE) != null) {
					throw new GraphValueException("Recursive structure detected!");
				}
				final JsonArray value = new JsonArray();
				for (final Object innerObject : collection) {
					value.add(createFromObject(innerObject, set));
				}
				set.remove(collection);
				yield value;
			}
			case Map<?, ?> map -> {
				final Map<Object, Boolean> set = identitySet != null ? identitySet : new IdentityHashMap<>();
				if (set.put(map, Boolean.FALSE) != null) {
					throw new GraphValueException("Recursive structure detected!");
				}
				final JsonObject value = new JsonObject();
				for (final Entry<?, ?> entry : map.entrySet()) {
					if (entry.getKey() instanceof CharSequence name) {
						final String string = name.toString();
						if (string.isEmpty()) throw new GraphValueException("Empty name is not allowed!");
						value.put(string, createFromObject(entry.getValue(), set));
					} else {
						throw new GraphValueException("Unexpected type of name!");
					}
				}
				set.remove(map);
				yield value;
			}
			default -> throw new GraphValueException("Unexpected type of value!");
		};
	}

	/**
	 * Check if JsonElement is valid for raw write.
	 */
	private static void checkElement(@Nullable JsonElement element,
			@Nullable Map<@NotNull Object, @NotNull Boolean> identitySet) {
		final Collection<JsonElement> elements = switch (element) {
			case JsonArray array -> array;
			case JsonObject object -> object.values();
			case null, default -> null;
		};
		if (elements == null) return;

		final Map<Object, Boolean> set = identitySet != null ? identitySet : new IdentityHashMap<>();
		if (set.put(element, Boolean.TRUE) != null) {
			throw new GraphValueException("Recursive structure detected!");
		}
		for (final JsonElement innerElement : elements) {
			checkElement(innerElement, set);
		}
		set.remove(element);
	}


	public final class ObjectWriter {
		private final @NotNull JsonObject object;


		public ObjectWriter(@NotNull JsonObject object) {
			this.object = object;
		}


		private static void checkDuplicate(@Nullable JsonElement element) {
			if (element != null) throw new GraphValueException("This key is already exists!");
		}

		private static void checkName(@NotNull String key) {
			if (key.isEmpty()) throw new GraphValueException("Key must not be empty!");
		}


		public void putJson(@NotNull String key, @Nullable JsonElement element) {
			checkName(key);
			checkElement(element, null);
			checkDuplicate(object.putIfAbsent(key, element != null ? element : JsonKeyword.NULL));
		}

		public void putRaw(@NotNull String key, @Nullable Object value) {
			checkName(key);
			checkDuplicate(object.putIfAbsent(key, createFromObject(value, null)));
		}

		public void putElement(@NotNull String key, @Nullable Element value) {
			checkName(key);
			checkDuplicate(object.put(key, value != null ? createFromElement(value) : JsonKeyword.NULL));
		}

		public @NotNull ArrayWriter putArray(@NotNull String key) {
			checkName(key);
			final JsonArray innerArray = new JsonArray();
			checkDuplicate(object.put(key, innerArray));
			return new ArrayWriter(innerArray);
		}

		public @NotNull ObjectWriter putObject(@NotNull String key) {
			checkName(key);
			final JsonObject innerObject = new JsonObject();
			checkDuplicate(object.put(key, innerObject));
			return new ObjectWriter(innerObject);
		}


		public void putValue(@NotNull String key, boolean value) {
			checkName(key);
			checkDuplicate(object.put(key, JsonElement.of(value)));
		}

		public void putValue(@NotNull String key, @Nullable Boolean value) {
			checkName(key);
			checkDuplicate(object.put(key, JsonElement.of(value)));
		}

		public void putValue(@NotNull String key, char value) {
			checkName(key);
			checkDuplicate(object.put(key, JsonElement.of(value)));
		}

		public void putValue(@NotNull String key, @Nullable Character value) {
			checkName(key);
			checkDuplicate(object.put(key, JsonElement.of(value)));
		}

		public void putValue(@NotNull String key, @Nullable String value) {
			checkName(key);
			checkDuplicate(object.put(key, JsonElement.of(value)));
		}

		public void putValue(@NotNull String key, long value) {
			checkName(key);
			checkDuplicate(object.put(key, JsonElement.of(value)));
		}

		public void putValue(@NotNull String key, @Nullable Long value) {
			checkName(key);
			checkDuplicate(object.put(key, JsonElement.of(value)));
		}

		public void putValue(@NotNull String key, double value) {
			checkName(key);
			checkDuplicate(object.put(key, JsonElement.of(value)));
		}

		public void putValue(@NotNull String key, @Nullable Double value) {
			checkName(key);
			checkDuplicate(object.put(key, JsonElement.of(value)));
		}

		public void putValue(@NotNull String key, @Nullable BigInteger value) {
			checkName(key);
			checkDuplicate(object.put(key, JsonElement.of(value)));
		}

		public void putValue(@NotNull String key, @Nullable BigDecimal value) {
			checkName(key);
			checkDuplicate(object.put(key, JsonElement.of(value)));
		}
	}

	public final class ArrayWriter {
		private final @NotNull JsonArray array;


		public ArrayWriter(@NotNull JsonArray array) {
			this.array = array;
		}


		public void addJson(@NotNull JsonElement element) {
			checkElement(element, null);
			array.add(element);
		}

		public void addRaw(@Nullable Object value) {
			array.add(createFromObject(value, null));
		}

		public void addElement(@Nullable Element value) {
			array.add(value != null ? createFromElement(value) : JsonKeyword.NULL);
		}

		public @NotNull ArrayWriter addArray() {
			final JsonArray innerArray = new JsonArray();
			array.add(innerArray);
			return new ArrayWriter(innerArray);
		}

		public @NotNull ObjectWriter addObject() {
			final JsonObject innerObject = new JsonObject();
			array.add(innerObject);
			return new ObjectWriter(innerObject);
		}


		public void addValue(boolean value) {
			array.add(JsonElement.of(value));
		}

		public void addValue(@Nullable Boolean value) {
			array.add(JsonElement.of(value));
		}

		public void addValue(char value) {
			array.add(JsonElement.of(value));
		}

		public void addValue(@Nullable Character value) {
			array.add(JsonElement.of(value));
		}

		public void addValue(@Nullable String value) {
			array.add(JsonElement.of(value));
		}

		public void addValue(long value) {
			array.add(JsonElement.of(value));
		}

		public void addValue(@Nullable Long value) {
			array.add(JsonElement.of(value));
		}

		public void addValue(double value) {
			array.add(JsonElement.of(value));
		}

		public void addValue(@Nullable Double value) {
			array.add(JsonElement.of(value));
		}

		public void addValue(@Nullable BigInteger value) {
			array.add(JsonElement.of(value));
		}

		public void addValue(@Nullable BigDecimal value) {
			array.add(JsonElement.of(value));
		}
	}
}
