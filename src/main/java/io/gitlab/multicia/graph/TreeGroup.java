/*
 * Copyright (C) 2021-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.graph;

import org.jetbrains.annotations.NotNull;

public class TreeGroup<T extends TreeNode> extends Group<T> {
	/**
	 * The signature of a {@link TreeGroup}.
	 */
	public static final @NotNull Signature SIGNATURE = Group.SIGNATURE.suffix("TreeGroup");


	/**
	 * Create a new {@link TreeGroup}.
	 *
	 * @param signature The signature of the {@link TreeGroup}.
	 * @param nodeClass The class of the contained {@link TreeNode}.
	 */
	public TreeGroup(@NotNull Signature signature, @NotNull Class<T> nodeClass) {
		super(signature, nodeClass);
		if (!signature.startsWith(SIGNATURE)) throw new GraphValueException("Invalid signature!");
	}
}
