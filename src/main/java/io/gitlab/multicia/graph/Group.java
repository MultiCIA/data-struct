/*
 * Copyright (C) 2021-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.graph;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public non-sealed class Group<N extends Node> extends ElementContainer<N> {
	/**
	 * The signature of a {@link Group}.
	 */
	public static final @NotNull Signature SIGNATURE = Signature.of("Group");


	/**
	 * Create a new {@link Group}.
	 *
	 * @param signature The signature of the {@link Group}.
	 * @param nodeClass The class of the contained {@link Node}.
	 */
	public Group(@NotNull Signature signature, @NotNull Class<N> nodeClass) {
		super(signature, nodeClass);
		if (!signature.startsWith(SIGNATURE)) throw new GraphValueException("Invalid signature!");
	}

	/**
	 * Create a new generic {@link Group} that contains the generic {@link Node}.
	 *
	 * @param signature The signature of the {@link Group}.
	 */
	Group(@NotNull Signature signature) {
		super(signature, Node.class);
		if (!signature.startsWith(SIGNATURE)) throw new GraphValueException("Invalid signature!");
	}


	/**
	 * The only {@link Group} that equal to this {@link Group} is this {@link Group} itself. This makes all collections
	 * of {@link Group}s effectively become identity collections.
	 *
	 * @param object object
	 * @return {@code this == object}
	 */
	@Override
	public final boolean equals(@Nullable Object object) {
		return this == object;
	}

	/**
	 * Return the object's default hashcode.
	 *
	 * @return {@code super.hashcode()}
	 */
	@Override
	public final int hashCode() {
		return super.hashCode();
	}
}
