/*
 * Copyright (C) 2021-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.graph;

import io.gitlab.multicia.graph.ElementHasher.FieldHasher;
import io.gitlab.multicia.graph.ElementMatcher.FieldMatcher;
import io.gitlab.multicia.graph.GraphDeserializer.ArrayReader;
import io.gitlab.multicia.graph.GraphDeserializer.ObjectReader;
import io.gitlab.multicia.graph.GraphSerializer.ArrayWriter;
import io.gitlab.multicia.graph.GraphSerializer.ObjectWriter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public final class Connections extends Entity implements Iterable<Connection> {
	/**
	 * The signature of a {@link Connections}.
	 */
	public static final @NotNull Signature SIGNATURE = Entity.SIGNATURE.suffix("Connections");

	/**
	 * The {@link FieldHasher} for the {@link Connection}s and their counts in the {@link Connections}. Notes that
	 * matched {@link Connection}s are merged for the calculation of the hash value.
	 */
	public static final @NotNull FieldHasher<@NotNull Connections> CONNECTION_COUNTS_HASHER
			= (hasher, connections) -> {
		final Map<ElementWrapper<?>, Long> map = new HashMap<>();
		for (final Connection connection : connections) {
			map.merge(hasher.wrap(connection), connections.getCount(connection), Long::sum);
		}
		return map.hashCode();
	};

	/**
	 * The {@link FieldMatcher} for the {@link Connection}s and their counts in the {@link Connections}. Notes that
	 * matched {@link Connection}s are merged for the comparison.
	 */
	public static final @NotNull FieldMatcher<@NotNull Connections, @NotNull Connections> CONNECTION_COUNTS_MATCHER
			= (matcher, connectionsA, connectionsB) -> {
		final HashMap<ElementWrapper<?>, Long> map = new HashMap<>();
		for (final Connection connection : connectionsA) {
			map.merge(matcher.wrap(connection), connectionsA.getCount(connection), Long::sum);
		}
		for (final Connection connection : connectionsB) {
			if (map.merge(matcher.wrap(connection), -connectionsB.getCount(connection), Long::sum) < 0) {
				return false;
			}
		}
		return map.values().stream().allMatch(value -> value == 0);
	};

	/**
	 * Contains inner {@link Connection} objects with their counts.
	 */
	private final @NotNull HashMap<@NotNull Connection, @NotNull Long> connectionCounts = new HashMap<>();


	/**
	 * Create a new {@link Connections}.
	 */
	public Connections() {
		super(SIGNATURE);
	}

	/**
	 * Create a new {@link Connections}.
	 *
	 * @param signature The signature of the {@link Connections}.
	 */
	Connections(@NotNull Signature signature) {
		super(SIGNATURE);
		if (!signature.equals(SIGNATURE)) throw new GraphValueException("Invalid signature!");
	}

	/**
	 * Create a new {@link Connections} by copying old connections. This is a shallow copy, as the inner
	 * {@link Connection} objects are not cloned.
	 */
	public Connections(@NotNull Connections connections) {
		super(SIGNATURE);
		connectionCounts.putAll(connections.connectionCounts);
	}


	/**
	 * Get the number of this type of {@link Connection}.
	 *
	 * @param connection Type of {@link Connection}.
	 * @return The number of this type of {@link Connection}.
	 */
	public long getCount(@NotNull Connection connection) {
		return connectionCounts.getOrDefault(connection, 0L);
	}

	/**
	 * Set the number of this type of {@link Connection}.
	 *
	 * @param connection Type of {@link Connection}.
	 * @param count The new count of this type of {@link Connection}.
	 */
	public void setCount(@NotNull Connection connection, long count) {
		if (count > 0L) {
			connectionCounts.put(connection, count);
		} else {
			connectionCounts.remove(connection);
		}
	}

	/**
	 * Add the number of this type of {@link Connection}.
	 *
	 * @param connection Type of {@link Connection}.
	 * @param delta The amount of this type of {@link Connection} to add.
	 */
	public void addCount(@NotNull Connection connection, long delta) {
		if (connectionCounts.merge(connection, delta, Long::sum) <= 0) connectionCounts.remove(connection);
	}

	/**
	 * Subtract the number of this type of {@link Connection}.
	 *
	 * @param connection Type of {@link Connection}.
	 * @param delta The amount of this type of {@link Connection} to subtract.
	 */
	public void subtractCount(@NotNull Connection connection, long delta) {
		if (connectionCounts.merge(connection, -delta, Long::sum) <= 0) connectionCounts.remove(connection);
	}

	/**
	 * Add all counts to this {@link Connections}.
	 *
	 * @param connections The {@link Connections}.
	 */
	public void addAllCounts(@NotNull Connections connections) {
		for (final Map.Entry<Connection, Long> entry : connections.connectionCounts.entrySet()) {
			connectionCounts.merge(entry.getKey(), entry.getValue(), Long::sum);
		}
	}

	/**
	 * Subtract all counts to this {@link Connections}.
	 *
	 * @param connections The {@link Connections}.
	 */
	public void subtractAllCounts(@NotNull Connections connections) {
		for (final Map.Entry<Connection, Long> entry : connections.connectionCounts.entrySet()) {
			final Connection connection = entry.getKey();
			if (connectionCounts.merge(connection, -entry.getValue(), Long::sum) <= 0) {
				connectionCounts.remove(connection);
			}
		}
	}


	@Override
	protected void writeToJson(@NotNull ObjectWriter writer) {
		super.writeToJson(writer);
		if (!connectionCounts.isEmpty()) {
			final ArrayWriter connectionCountsWriter = writer.putArray("connectionCounts");
			for (final Map.Entry<Connection, Long> entry : connectionCounts.entrySet()) {
				connectionCountsWriter.addElement(entry.getKey());
				connectionCountsWriter.addValue(entry.getValue());
			}
		}
	}

	@Override
	protected void readFromJson(@NotNull ObjectReader reader) {
		super.readFromJson(reader);
		final ArrayReader connectionCountsReader = reader.getAsArray("connectionCounts");
		if (connectionCountsReader != null) {
			final int size = connectionCountsReader.size();
			if ((size & 1) != 0) throw new GraphValueException("Invalid connections array size!");
			for (int index = 0; index < size; index += 2) {
				final Connection connection = connectionCountsReader.getAsElement(index, Connection.class);
				if (connection == null) throw new GraphValueException("Unexpected null value!");
				final Long count = connectionCountsReader.getAsLong(index + 1);
				if (count == null) throw new GraphValueException("Unexpected null value!");
				setCount(connection, count);
			}
		}
	}


	/**
	 * Returns an iterator of {@link Connection}s.
	 *
	 * @return an {@link Iterator}.
	 */
	@Override
	public @NotNull Iterator<@NotNull Connection> iterator() {
		return new ImmutableIterator<>(connectionCounts.keySet().iterator());
	}


	@Override
	public @NotNull String toString() {
		return connectionCounts.toString();
	}

	@Override
	public boolean equals(@Nullable Object object) {
		return this == object || object instanceof Connections connections
				&& connectionCounts.equals(connections.connectionCounts);
	}

	@Override
	public int hashCode() {
		return connectionCounts.hashCode();
	}


	private record ImmutableIterator<E>(@NotNull Iterator<E> iterator) implements Iterator<E> {
		@Override
		public boolean hasNext() {
			return iterator.hasNext();
		}

		@Override
		public E next() {
			return iterator.next();
		}
	}
}
