/*
 * Copyright (C) 2021-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.graph;

import io.gitlab.multicia.jsonify.JsonArray;
import io.gitlab.multicia.jsonify.JsonElement;
import io.gitlab.multicia.jsonify.JsonNumber;
import io.gitlab.multicia.jsonify.JsonObject;
import io.gitlab.multicia.jsonify.JsonRuntimeException;
import io.gitlab.multicia.jsonify.JsonString;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Unmodifiable;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SequencedMap;
import java.util.Set;

public final class GraphDeserializer {
	private static final @NotNull Map<@NotNull Signature, @NotNull ElementConstructor> DEFAULT_CONSTRUCTORS = Map.of(
			Graph.SIGNATURE, Graph::new,
			Group.SIGNATURE, Group::new,
			Node.SIGNATURE, Node::new,
			Entity.SIGNATURE, Entity::new,
			Connection.SIGNATURE, Connection::new,
			Connections.SIGNATURE, Connections::new
	);

	private final @NotNull Map<@NotNull Signature, @NotNull ElementConstructor> constructors
			= new HashMap<>(DEFAULT_CONSTRUCTORS);

	private final @NotNull Map<@NotNull JsonArray, @NotNull Entry<@NotNull Element, @NotNull ObjectReader>>
			referenceArrayMap = new HashMap<>();


	private GraphDeserializer(@NotNull Map<@NotNull Signature, @NotNull ElementConstructor> additionalConstructors) {
		constructors.putAll(additionalConstructors);
	}


	public static @NotNull Graph deserialize(@NotNull JsonElement element,
			@NotNull Map<@NotNull Signature, @NotNull ElementConstructor> constructors) throws GraphException {
		try {
			final GraphDeserializer deserializer = new GraphDeserializer(constructors);
			return deserializer.deserialize(element);
		} catch (final JsonRuntimeException | GraphValueException exception) {
			throw new GraphException("Cannot deserialize this JSON to Graph!", exception);
		}
	}

	private @NotNull Graph deserialize(@NotNull JsonElement element) {
		// split into 2 pass
		// 1. Create all referencable Element from their JsonObject
		if (!(element instanceof JsonObject graphObject)) throw new GraphValueException("Unexpected value!");
		final ObjectReader reader = new ObjectReader(graphObject);
		final Element graphElement = reader.constructElement(new ArrayList<>());
		if (!(graphElement instanceof Graph graph)) throw new GraphValueException("Unexpected value!");
		// 2. Deserialize all JsonObject to corresponding Element
		for (final Entry<Element, ObjectReader> entry : referenceArrayMap.values()) {
			entry.getValue().readElement(entry.getKey());
		}
		return graph;
	}


	private @NotNull Element constructElementFromSignature(@NotNull String signatureString) {
		final Signature signature = Signature.parse(signatureString);
		Signature parentSignature = signature;
		do {
			final ElementConstructor constructor = constructors.get(parentSignature);
			final Element element = constructor != null ? constructor.construct(signature) : null;
			if (element != null) return element;
			parentSignature = parentSignature.parent();
		} while (parentSignature != null);
		throw new GraphValueException("Unknown signature!");
	}

	private @NotNull Element getElementFromJsonObject(@NotNull JsonObject object) {
		final JsonElement signatureElement = object.get("");
		if (signatureElement instanceof JsonArray referenceArray) {
			// get element from reference
			final Entry<Element, ObjectReader> entry = referenceArrayMap.get(referenceArray);
			if (entry == null) throw new GraphValueException("Invalid reference to element!");
			return entry.getKey();
		} else if (signatureElement instanceof JsonString signatureString) {
			// read signature & construct from signature
			final Element element = constructElementFromSignature(signatureString.toString());
			final ObjectReader reader = new ObjectReader(object);
			reader.readElement(element);
			return element;
		}
		throw new GraphValueException("Invalid element!");
	}


	public final class ObjectReader {
		private final @NotNull SequencedMap<String, JsonElement> object;


		private ObjectReader(@NotNull JsonObject object) {
			this.object = new LinkedHashMap<>(object);
		}


		private static void checkName(@NotNull String key) {
			if (key.isEmpty()) throw new IllegalArgumentException("Key must not be empty!");
		}

		public @Unmodifiable @NotNull Set<@NotNull String> getKeys() {
			return Set.copyOf(object.keySet());
		}


		public @Nullable Boolean getAsBoolean(@NotNull String key) {
			checkName(key);
			return JsonElement.toBoolean(object.remove(key));
		}

		public @Nullable String getAsString(@NotNull String key) {
			checkName(key);
			return JsonElement.toString(object.remove(key));
		}

		public @Nullable Character getAsCharacter(@NotNull String key) {
			checkName(key);
			return JsonElement.toCharacter(object.remove(key));
		}

		public @Nullable Number getAsNumber(@NotNull String key) {
			checkName(key);
			return JsonElement.toNumber(object.remove(key));
		}

		public @Nullable Long getAsLong(@NotNull String key) {
			checkName(key);
			return JsonElement.toLong(object.remove(key));
		}

		public @Nullable Double getAsDouble(@NotNull String key) {
			checkName(key);
			return JsonElement.toDouble(object.remove(key));
		}

		public @Nullable BigInteger getAsBigInteger(@NotNull String key) {
			checkName(key);
			return JsonElement.toBigInteger(object.remove(key));
		}

		public @Nullable BigDecimal getAsBigDecimal(@NotNull String key) {
			checkName(key);
			return JsonElement.toBigDecimal(object.remove(key));
		}


		public @Nullable ArrayReader getAsArray(@NotNull String key) {
			checkName(key);
			final JsonArray innerArray = JsonElement.toJsonArray(object.remove(key));
			return innerArray != null ? new ArrayReader(innerArray) : null;
		}

		public @Nullable ObjectReader getAsObject(@NotNull String key) {
			checkName(key);
			final JsonObject innerObject = JsonElement.toJsonObject(object.remove(key));
			if (innerObject == null) return null;
			if (!innerObject.containsKey("")) return new ObjectReader(innerObject);
			throw new GraphValueException("Cannot get an Element as a normal Object!");
		}

		public @Nullable Element getAsElement(@NotNull String key) {
			checkName(key);
			final JsonElement element = object.remove(key);
			if (element == null) return null;
			final JsonObject object = element.asJsonObject();
			if (object.containsKey("")) return getElementFromJsonObject(object);
			throw new GraphValueException("Cannot get a normal Object as an Element!");
		}

		public <E extends Element> @Nullable E getAsElement(@NotNull String key, @NotNull Class<E> elementClass) {
			final Element node = getAsElement(key);
			if (node == null) return null;
			if (elementClass.isInstance(node)) return elementClass.cast(node);
			throw new GraphValueException("Incorrect type of Element!");
		}


		private <E extends Element> void constructInnerElements(@NotNull ElementContainer<@NotNull E> container,
				@NotNull List<@NotNull Long> referenceIndexes) {
			// get inner elements
			final JsonArray array = JsonElement.toJsonArray(object.remove("elements"));
			if (array == null) return; // success: empty elements
			// mark position of new index value
			final int position = referenceIndexes.size();
			referenceIndexes.add(0L);
			// loop though all element
			final Class<E> elementClass = container.getElementClass();
			for (int index = 0, length = array.size(); index < length; index++) {
				// get element
				final JsonObject object = JsonElement.toJsonObject(array.get(index));
				if (object == null) throw new GraphValueException("ElementContainer cannot contain null Element!");
				// construct element with reference index
				referenceIndexes.set(position, (long) index);
				final ObjectReader reader = new ObjectReader(object);
				final Element element = reader.constructElement(referenceIndexes);
				// add element to elements
				if (!elementClass.isInstance(element)) throw new GraphValueException("Unexpected type of Element!");
				container.add(elementClass.cast(element));
			}
			// remove new index value
			referenceIndexes.remove(position);
		}

		private @NotNull Element constructElement(@NotNull List<@NotNull Long> referenceIndexes) {
			// read signature & construct from signature
			final String signatureString = JsonElement.toString(object.remove(""));
			if (signatureString == null) throw new GraphValueException("Element signature must not be null!");
			final Element element = constructElementFromSignature(signatureString);
			// if it contains elements, recursively construct from inner json object
			if (element instanceof ElementContainer<?> container) {
				constructInnerElements(container, referenceIndexes);
			}
			// create reference to this & later read from json
			final JsonArray referenceArray = new JsonArray();
			for (final Long referenceIndex : referenceIndexes) {
				referenceArray.add(new JsonNumber(referenceIndex));
			}
			referenceArrayMap.put(referenceArray, Map.entry(element, this));
			return element;
		}


		private @Nullable Object getRawFromJsonElement(@Nullable JsonElement jsonElement) {
			if (jsonElement instanceof JsonArray jsonArray) {
				final Collection<Object> list = new ArrayList<>(jsonArray.size());
				for (final JsonElement innerElement : jsonArray) {
					list.add(getRawFromJsonElement(innerElement));
				}
				return list;
			} else if (jsonElement instanceof JsonObject jsonObject) {
				if (jsonObject.containsKey("")) return getElementFromJsonObject(jsonObject);

				final Map<String, Object> map = new LinkedHashMap<>();
				for (final Entry<String, JsonElement> entry : jsonObject.entrySet()) {
					map.put(entry.getKey(), getRawFromJsonElement(entry.getValue()));
				}
				return map;
			}
			return jsonElement;
		}

		private void readElement(@NotNull Element element) {
			element.readFromJson(this);
			// read remaining fields
			if (!object.isEmpty()) {
				final Map<String, Object> map = new LinkedHashMap<>();
				for (final Entry<String, JsonElement> entry : object.entrySet()) {
					map.put(entry.getKey(), getRawFromJsonElement(entry.getValue()));
				}
				element.setUnknownFields(map);
			}
		}
	}

	public final class ArrayReader {
		private final @NotNull JsonArray array;


		private ArrayReader(@NotNull JsonArray array) {
			this.array = array;
		}


		public int size() {
			return array.size();
		}


		public @Nullable Boolean getAsBoolean(int index) {
			return JsonElement.toBoolean(array.get(index));
		}

		public @Nullable String getAsString(int index) {
			return JsonElement.toString(array.get(index));
		}

		public @Nullable Character getAsCharacter(int index) {
			return JsonElement.toCharacter(array.get(index));
		}

		public @Nullable Number getAsNumber(int index) {
			return JsonElement.toNumber(array.get(index));
		}

		public @Nullable Long getAsLong(int index) {
			return JsonElement.toLong(array.get(index));
		}

		public @Nullable Double getAsDouble(int index) {
			return JsonElement.toDouble(array.get(index));
		}

		public @Nullable BigInteger getAsBigInteger(int index) {
			return JsonElement.toBigInteger(array.get(index));
		}

		public @Nullable BigDecimal getAsBigDecimal(int index) {
			return JsonElement.toBigDecimal(array.get(index));
		}


		public @Nullable ArrayReader getAsArray(int index) {
			final JsonArray innerArray = JsonElement.toJsonArray(array.get(index));
			return innerArray != null ? new ArrayReader(innerArray) : null;
		}

		public @Nullable ObjectReader getAsObject(int index) {
			final JsonObject innerObject = JsonElement.toJsonObject(array.get(index));
			if (innerObject == null) return null;
			if (!innerObject.containsKey("")) return new ObjectReader(innerObject);
			throw new GraphValueException("Cannot get an Element as a normal Object!");
		}

		public @Nullable Element getAsElement(int index) {
			final JsonObject innerObject = JsonElement.toJsonObject(array.get(index));
			if (innerObject == null) return null;
			if (innerObject.containsKey("")) return getElementFromJsonObject(innerObject);
			throw new GraphValueException("Cannot get a normal Object as an Element!");
		}

		public <E extends Element> @Nullable E getAsElement(int index, @NotNull Class<E> nodeClass) {
			final Element node = getAsElement(index);
			if (node == null) return null;
			if (nodeClass.isInstance(node)) return nodeClass.cast(node);
			throw new GraphValueException("Incorrect type of Element!");
		}
	}
}
