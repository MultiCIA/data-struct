/*
 * Copyright (C) 2021-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.graph;

import io.gitlab.multicia.graph.ElementHasher.FieldHasher;
import io.gitlab.multicia.graph.ElementMatcher.FieldMatcher;
import io.gitlab.multicia.graph.GraphDeserializer.ObjectReader;
import io.gitlab.multicia.graph.GraphSerializer.ObjectWriter;
import org.jetbrains.annotations.MustBeInvokedByOverriders;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * This class define an {@link Element}, the base class of the data structure. An {@link Element} is consist of a
 * {@link Signature} and other fields, depends on the specific type of {@link Element}.
 */
public sealed abstract class Element permits ElementContainer, Entity, Node {
	/**
	 * The {@link FieldHasher} for the {@link Signature} in the {@link Element}.
	 */
	public static final @NotNull FieldHasher<@NotNull Element> SIGNATURE_HASHER
			= (hasher, element) -> element.getSignature().hashCode();

	/**
	 * The {@link FieldMatcher} for the {@link Signature} in the {@link Element}.
	 */
	public static final @NotNull FieldMatcher<@NotNull Element, @NotNull Element> SIGNATURE_MATCHER
			= (matcher, elementA, elementB) -> elementA.getSignature().equals(elementB.getSignature());

	/**
	 * Contains the signature of this {@link Element} object.
	 * <p>
	 * Note: This field is handled specially by the JSON serializer/deserializer.
	 */
	private final @NotNull Signature signature;

	/**
	 * Contains unknown fields when serialize/deserialize. These unknown fields can be read and/or modified.
	 * <p>
	 * Note: This field is handled specially by the JSON serializer/deserializer.
	 */
	protected @NotNull Map<@NotNull String, @NotNull Object> unknownFields = Map.of();


	/**
	 * Create a new {@link Element}.
	 *
	 * @param signature The signature of the {@link Element}.
	 */
	Element(@NotNull Signature signature) {
		this.signature = signature;
	}


	/**
	 * @return The {@link Signature} of this {@link Element} object.
	 */
	public final @NotNull Signature getSignature() {
		return signature;
	}


	/**
	 * @return Unknown fields as a {@link Map} of {@link String} to {@link Object}.
	 */
	public final @NotNull Map<@NotNull String, @NotNull Object> getUnknownFields() {
		return unknownFields;
	}

	/**
	 * @param unknownFields The {@link Map} of {@link String} to {@link Object} as a new unknown fields.
	 */
	public final void setUnknownFields(@NotNull Map<@NotNull String, @NotNull Object> unknownFields) {
		this.unknownFields = unknownFields;
	}


	/**
	 * Write this object to the output JSON object through an {@link ObjectWriter}.
	 *
	 * @param writer The target {@link ObjectWriter}.
	 */
	@MustBeInvokedByOverriders
	protected void writeToJson(@NotNull ObjectWriter writer) {
	}

	/**
	 * Read this object from the input JSON object through an {@link ObjectReader}.
	 *
	 * @param reader The source {@link ObjectReader}.
	 */
	@MustBeInvokedByOverriders
	protected void readFromJson(@NotNull ObjectReader reader) {
	}
}
