/*
 * Copyright (C) 2021-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.graph;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public final class Signature {
	private static final @NotNull Pattern SIGNATURE_VALID_PATTERN = Pattern.compile("\\w+(?::\\w+)*");
	private static final @NotNull Pattern ELEMENT_VALID_PATTERN = Pattern.compile("\\w+");

	private final @NotNull String @NotNull [] elements;


	private Signature(@NotNull String @NotNull [] elements) {
		this.elements = elements;
	}


	private static boolean isValidElement(@NotNull String element) {
		return ELEMENT_VALID_PATTERN.matcher(element).matches();
	}

	private static boolean isValidSignature(@NotNull String signature) {
		return SIGNATURE_VALID_PATTERN.matcher(signature).matches();
	}

	private static boolean isValidElements(@NotNull String @NotNull ... elements) {
		for (final String element : elements) {
			if (!isValidElement(element)) {
				return false;
			}
		}
		return true;
	}


	public static @NotNull Signature parse(@NotNull String signature) {
		if (isValidSignature(signature)) {
			return new Signature(signature.split(":"));
		} else {
			throw new GraphValueException("Invalid signature!");
		}
	}

	public static @NotNull Signature of(@NotNull String @NotNull ... elements) {
		if (elements.length > 0 && isValidElements(elements)) {
			return new Signature(elements.clone());
		} else {
			throw new GraphValueException("Invalid signature!");
		}
	}


	public boolean startsWith(@NotNull String string) {
		return elements[0].equals(string);
	}

	public boolean startsWith(@NotNull String @NotNull ... strings) {
		final int length = strings.length;
		if (length == 0) throw new GraphValueException("Invalid signature!");
		if (length > elements.length) return false;
		for (int index = 0; index < length; index++) {
			if (!strings[index].equals(elements[index])) {
				return false;
			}
		}
		return true;
	}

	public boolean startsWith(@NotNull Signature signature) {
		return startsWith(signature.elements);
	}

	public boolean endsWith(@NotNull String string) {
		return elements[elements.length - 1].equals(string);
	}

	public boolean endsWith(@NotNull String @NotNull ... strings) {
		final int length = strings.length;
		final int elementsLength = elements.length;
		if (length == 0) throw new GraphValueException("Invalid signature!");
		if (length > elementsLength) return false;
		for (int index = 0; index < length; index++) {
			if (!strings[index].equals(elements[elementsLength - length + index])) {
				return false;
			}
		}
		return true;
	}

	public boolean endsWith(@NotNull Signature signature) {
		return endsWith(signature.elements);
	}


	public @NotNull Signature prefix(@NotNull String string) {
		if (isValidElement(string)) {
			return new Signature(Stream.concat(Stream.of(string), Stream.of(elements)).toArray(String[]::new));
		} else {
			throw new GraphValueException("Invalid signature!");
		}
	}

	public @NotNull Signature prefix(@NotNull String @NotNull ... strings) {
		if (isValidElements(strings)) {
			return new Signature(Stream.concat(Stream.of(strings), Stream.of(elements)).toArray(String[]::new));
		} else {
			throw new GraphValueException("Invalid signature!");
		}
	}

	public @NotNull Signature suffix(@NotNull String string) {
		if (isValidElement(string)) {
			return new Signature(Stream.concat(Stream.of(elements), Stream.of(string)).toArray(String[]::new));
		} else {
			throw new GraphValueException("Invalid signature!");
		}
	}

	public @NotNull Signature suffix(@NotNull String @NotNull ... strings) {
		if (isValidElements(strings)) {
			return new Signature(Stream.concat(Stream.of(elements), Stream.of(strings)).toArray(String[]::new));
		} else {
			throw new GraphValueException("Invalid signature!");
		}
	}


	public @Nullable Signature parent() {
		return elements.length > 1
				? new Signature(Arrays.copyOfRange(elements, 0, elements.length - 1))
				: null;
	}


	@Override
	public boolean equals(@Nullable Object object) {
		return this == object || object instanceof Signature signature && Arrays.equals(elements, signature.elements);
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(elements);
	}

	@Override
	public @NotNull String toString() {
		return String.join(":", elements);
	}
}
