/*
 * Copyright (C) 2021-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.graph;

import io.gitlab.multicia.graph.GraphDiffer.TypeDiffer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Wraps an {@link Element} using a specific {@link MatchingType}. The {@link Object#equals(Object)} is implemented via
 * {@link GraphMatcher#match(Element, Element)}, and {@link Object#hashCode()} is implemented via
 * {@link GraphHasher#hash(Element)}.
 *
 * @param <E> The {@link Element} type.
 */
public final class ElementWrapper<E extends Element> {
	private final @NotNull TypeDiffer typeDiffer;
	private final @Nullable E element;
	private final int hashCode;

	ElementWrapper(@NotNull TypeDiffer typeDiffer, @Nullable E element) {
		this.typeDiffer = typeDiffer;
		this.element = element;
		this.hashCode = typeDiffer.hash(element);
	}

	public @Nullable E get() {
		return element;
	}

	@Override
	public boolean equals(@Nullable Object object) {
		return this == object || object instanceof ElementWrapper<?> wrapper
				&& hashCode == wrapper.hashCode && typeDiffer.match(element, wrapper.element);
	}

	@Override
	public int hashCode() {
		return hashCode;
	}
}
