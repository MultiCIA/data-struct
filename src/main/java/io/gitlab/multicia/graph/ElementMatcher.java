/*
 * Copyright (C) 2021-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.graph;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * {@link ElementMatcher} compares a pair of {@link Element}s. This class should be extended by the user to implement
 * the way {@link Element}s are being compared. Implementer must make sure that any matching {@link Element}s with the
 * same {@link MatchingType} will have the same hash value. Any dependency to parent type {@link ElementMatcher} should
 * be called directly. Any comparison for other {@link Element}s or other type of {@link MatchingType} should be called
 * through {@link GraphMatcher}.
 *
 * @param <A> Any {@link Element} type.
 * @param <B> Any {@link Element} type.
 */
public record ElementMatcher<A extends Element, B extends Element>(
		@NotNull MatchingType matchingType,
		@NotNull Class<A> elementClassA,
		@NotNull Class<B> elementClassB,
		@NotNull List<@NotNull FieldMatcher<? super A, ? super B>> fieldMatchers
) {
	/**
	 * Compare two {@link Element}s.
	 *
	 * @param matcher The {@link GraphMatcher}.
	 * @param elementA The first {@link Element}.
	 * @param elementB The second {@link Element}.
	 * @return {@code true} if the first {@link Element} matches the second {@link Element}.
	 */
	public boolean match(@NotNull GraphMatcher matcher, @NotNull A elementA, @NotNull B elementB) {
		for (final FieldMatcher<? super A, ? super B> fieldMatcher : fieldMatchers) {
			if (!fieldMatcher.match(matcher, elementA, elementB)) {
				return false;
			}
		}
		return true;
	}


	@FunctionalInterface
	public interface FieldMatcher<A extends Element, B extends Element> {
		/**
		 * Compare some fields in two {@link Element}s.
		 *
		 * @param matcher The {@link GraphMatcher}.
		 * @param elementA The first {@link Element}.
		 * @param elementB The second {@link Element}.
		 * @return {@code true} if those specified fields are matched.
		 */
		boolean match(@NotNull GraphMatcher matcher, @NotNull A elementA, @NotNull B elementB);
	}
}
